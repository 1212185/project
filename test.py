from vgg16 import Vgg16

path = '../dogscats'

vgg = Vgg16()

batch_size = 64

batches = vgg.get_batches(path + 'train',batch_size=batch_size)
val_batches = vgg.get_batches(path + 'valid',batch_size=batch_size)

vgg.finetune(batches)

vgg.fit(batches, val_batches, nb_epoch = 10)